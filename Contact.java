import java.util.ArrayList;
import java.util.Iterator;

public class Contact {
    private ArrayList<Group> groups = new ArrayList<>();
    private ArrayList<Person> persons = new ArrayList<>();
    private int lastId = 0;
    private int lastGroupId = 0;

    public Contact() {
        groups.add(new Group(String.valueOf(lastGroupId), "All"));
    }

    // View all person in contact
    public String getAll() {
        StringBuilder stringBuilder = new StringBuilder("Contact List : \n");

        if (persons.size() > 0) {
            for (Person person : persons) {
                stringBuilder.append(String.format("%s\n\n", person.toString()));
            }
        } else {
            stringBuilder.append("Don't have any contact, please add new\n");
        }

        return stringBuilder.toString();
    }

    public String getAllByGroup(String groupId) {
        StringBuilder stringBuilder = new StringBuilder();
        if (searchGroupById(groupId)) {
            stringBuilder.append(String.format("Contact List with id '%s' :\n", groupId));
            for (Person person : persons) {
                if (person.memberOf(groupId)) {
                    stringBuilder.append(String.format("%s\n\n", person.toString()));
                }
            }
        } else {
            stringBuilder.append("Group's id doesn't exist.\n");
        }

        return stringBuilder.toString();
    }

    // Add person
    public boolean add(String name, String gender) {
        return persons.add(new Person(String.valueOf(++lastId), name, Gender.setValue(gender)));
    }

    public boolean addWithGroup(String name, String gender, String groupsId) {
        ArrayList<String> separatedId = new ArrayList<>();
        boolean isAdded = false;
        boolean isGroupExists = true;

        for (String id : groupsId.split(",")) {
            isGroupExists = searchGroupById(id);
            if (isGroupExists) {
                separatedId.add(id);
            }
        }

        if (isGroupExists) {
            Person person = new Person(String.valueOf(++lastId), name, Gender.setValue(gender));
            person.addGroups(separatedId);
            isAdded = persons.add(person);

        }

        return isAdded && isGroupExists;
    }

    // Delete person
    public boolean deleteById(String id) {
        return persons.removeIf(persons -> persons.getId().equals(id));
    }

    public String getAllGroup() {
        StringBuilder stringBuilder = new StringBuilder("Group List : \n");
        for (Group group : groups) {
            stringBuilder.append(group.toString());
        }
        return stringBuilder.toString();
    }

    public boolean searchGroupById(String id) {
        boolean isExist = false;
        for (Group group : groups) {
            if (group.getId().equals(id)) {
                isExist = true;
                break;
            }
        }

        return isExist;
    }

    public boolean addGroup(String name) {
        return groups.add(new Group(String.valueOf(++lastGroupId), name));
    }

    public boolean deleteGroupById(String id) {
        boolean isDeleted = false;

        for (Iterator<Group> groupsIterator = groups.iterator(); groupsIterator.hasNext(); ){
            Group group = groupsIterator.next();
            if (group.getId().equals(id) && !group.getId().equals("0")) {
                groupsIterator.remove();
                isDeleted = true;
                break;
            }
        }

        return isDeleted;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Person person : persons) {
            stringBuilder.append(String.format("%s %s %s %s\n", person.getId(), person.getName(), person.getGender(), getGroupsString(person.getGroupsIdString())));
        }
        
        return stringBuilder.toString();
    }

    private String getGroupsString(String groupsIdString) {
        String[] groupsId = groupsIdString.split(",");
        String names = "";
        for (int i = 0; i < groupsId.length; i++) {
            for (Group group : groups) {
                if (group.getId().equals(groupsId[i])) {
                    names += group.getName();
                    break;
                }
            }

            names += i == groupsId.length - 1 ? "" : ",";
        }
        return names;
    }
    
}
