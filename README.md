# formative6

### Nexsoft Fun Coding Bootcamp Batch 7 Formative Day 7

## Simple Contact App
### Version 1.3


## How to run
### Windows
1. Just execute 'compileAndRun.bat'
2. Use Command prompt or terminal
```
java Main.java
```

### Linux
1. Use terminal
```
java Main.java
```

## Running program
1. Main Menu (user input start with '>')
```
====Simple Contact App====
1. Contact
2. Group
0. Exit
Choice [1/2/0] : >
```

2. Submenu Contact (user input start with '>')
```
====Menu Group====
1. Show Contact
2. Add Contact
3. Delete Contact
4. Search By Group Id
0. Back
Choice [1/2/3/4/0] : >
```

3. Submenu Group (user input start with '>')
```
====Menu Group====
1. Show Group
2. Add New Group
3. Delete Group
0. Back
Choice [1/2/3/0] : >
```

4. Show all contact
```
Contact List :
ID : 1
Name : Bayu Seno Ariefyanto
Gender : Male
Groups : [0]
```

5. Add new contact (user input start with '>')
```
====Add New Contact====
Input Name : Akbar Widyoseno
Gender [male/female] : male
Groups [single : '1' or multiple : '1,2,3,4'] without quotation, default '0' : 0,1
[Please input existing group's id]
Groups [single : '1' or multiple : '1,2,3,4'] without quotation, default '0' : 0
Input new contact successfully.
```

6. Search by Group id
```
====Get All By Id Contact====
Contact ID [1/2/etc] : 1
Contact List with id '1' :
ID : 4
Name : Bayu Seno Ariefyanto
Gender : Male
Groups : [0, 1]

ID : 5
Name : Fransisca
Gender : Female
Groups : [0, 1]

ID : 6
Name : Devida
Gender : Female
Groups : [0, 1]

ID : 7
Name : Kenny
Gender : Male
Groups : [0, 1]
```

7. ...