import java.util.ArrayList;

public class Person {
    private String id;
    private String name;
    private Gender gender;
    private ArrayList<String> groupsId = new ArrayList<>();

    public Person(String id, String name, Gender gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }

    public Person(String id, String name, Gender gender, String groupId) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.groupsId.add(groupId);
    }

    public void addGroups(ArrayList<String> groupsId) {
        this.groupsId.addAll(groupsId);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }

    public String getGroupsIdString() {
        return groupsId.toString().replaceAll("\\[|\\]| ", "");
    }

    public boolean memberOf(String groupId) {
        boolean isMember = false;
        for (String id : groupsId) {
            if (id.equals(groupId)) {
                isMember = true;
                break;
            }
        }

        return isMember;
    }

    @Override
    public String toString() {
        return String.format("ID : %s\nName : %s\nGender : %s\nGroups : %s", id, name, gender, groupsId.toString());
    }
}