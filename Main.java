import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;


public class Main {
    private File mainDirectory = new File("./data");
    private Scanner scanner;
    private int menuSelection;
    private Contact contact;

    public Main() {
        scanner = new Scanner(System.in);
        contact = new Contact();
    }

    public void run() throws Exception {

        do {
            showMainMenu();
            try {
                menuSelection = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                menuSelection = 0;
            }

            switch (menuSelection) {
                // Sub Menu contact
                case 1:
                    do {
                        showContactMenu();
                        try {
                            menuSelection = Integer.parseInt(scanner.nextLine());
                        } catch (NumberFormatException e) {
                            menuSelection = 0;
                        }

                        switch (menuSelection) {
                            case 1:
                                showContact();
                                break;
                            case 2:
                                addContact();
                                break;
                            case 3: 
                                deleteContact();
                                break;
                            case 4:
                                showContactByGroup();
                                break;
                            default:
                                menuSelection = 0;
                                break;
                        }
                    } while (menuSelection != 0);
                    
                    break;
                // Sub Menu group
                case 2:
                    do {
                        showGroupMenu();
                        try {
                            menuSelection = Integer.parseInt(scanner.nextLine());
                        } catch (NumberFormatException e) {
                            menuSelection = 0;
                        }

                        switch (menuSelection) {
                            case 1:
                                clearScreen();
                                print(contact.getAllGroup());
                                break;
                            case 2:
                                addContactGroup();
                                break;
                            case 3:
                                deleteContactGroup();
                                break;
                            default:
                                menuSelection = 0;
                                break;
                        }
                    } while (menuSelection != 0);
                    break;
                case 3:
                    exportContact();
                    break;
                default: 
                    menuSelection = -1;
                    break;
            }
        } while (menuSelection != -1);
    }

    private void addContact() {
        String name;
        String genderChoice;
        String ids;
        boolean isNameValid;
        boolean isGenderValid;
        boolean isIdsValid;
        boolean isSuccess;
        clearScreen();
        println("====Add New Contact====");
        do {
            print("Input Name : ");
            name = scanner.nextLine();
            isNameValid = name.matches("[A-Za-z ?]{4,}");

            if (!isNameValid) {
                println("[Please alphabets only and 4 or more character]");
            }

        } while (!isNameValid);
        
        do {
            print("Gender [male/female] : ");
            genderChoice = scanner.nextLine();
            isGenderValid = genderChoice.matches("(fe)?male");

            if (!isGenderValid) {
                println("[Please input female or male lowercase only]");
            }

        } while (!isGenderValid);

        do {
            print("Groups [single : '1' or multiple : '1,2,3,4'] without quotation, default '0' : ");
            ids = scanner.nextLine();
            isIdsValid = ids.matches("(\\d)+(,\\d+)*");
            isSuccess = isIdsValid ? contact.addWithGroup(name, genderChoice, ids) : false;

            if(!isSuccess) {
                println("[Please input existing group's id]");
            }

        } while (!isIdsValid || !isSuccess);

        println("Input new contact successfully.");
    }

    private void showContact() {
        clearScreen();       
        print(contact.getAll());
    }

    private void deleteContact() {
        String id;
        clearScreen();
        println("====Delete Contact====");
        print("Contact ID [1/2/etc] : ");
        
        id = scanner.nextLine();
        boolean isDeleted = contact.deleteById(id);
        if (isDeleted) {
            println("Contact with id : " + id + ", deleted successfully");
        } else {
            println("Contact with id : " + id + ", failed to delete.");
        }
    }

    private void showContactByGroup() {
        String id;
        clearScreen();
        println("====Get All By Id Contact====");
        print("Contact ID [1/2/etc] : ");
        id = scanner.nextLine();
        print(contact.getAllByGroup(id));
    }

    private void addContactGroup() {
        String name;
        clearScreen();
        println("====Add New Contact Group====");
        print("Input Group Name : ");
        name = scanner.nextLine();

        if (contact.addGroup(name)) {
            println("Input new group successfully.");
        } else {
            println("Failed input new group.");
        }
    }

    private void deleteContactGroup() {
        String id;
        println("====Delete Groups====");
        print("Group ID [1/2/etc] : ");
        id = scanner.nextLine();

        if (contact.deleteGroupById(id)) {
            println("Group with id : " + id + ", deleted successfully");
        } else {
            println("Group with id : " + id + " doesn't exist, failed to delete.");
        }
    }

    private void exportContact() {

        try {
            if (mainDirectory.mkdir()) {
                println(String.format("[Directory %s created]", mainDirectory.getName()));
            } else {
                println(String.format("[Directory %s exist, using existing directory]", mainDirectory.getName()));
            };
        } catch(Exception e) {
            println("[Error create directory, please restart a program]");
        }

        try {
            Path path = Paths.get(mainDirectory.toString(), "contacts.txt");
            if (path.toFile().exists()) {
                path.toFile().delete();
            }
            
            Files.writeString(path, contact.toString(), StandardOpenOption.CREATE);
        } catch (Exception e) {
            println("[Error write file, please restart a program]");
        }

        println("[Export contact successfully]");
    }

    private void showMainMenu() {
        System.out.println("====Simple Contact App====");
        System.out.print("1. Contact\n2. Group\n3. Export Contact\n0. Exit\nChoice [1/2/3/0] : ");
    }

    private void showContactMenu() {
        System.out.println("====Menu Group====");
        System.out.println("1. Show Contact\n2. Add Contact\n3. Delete Contact\n4. Search By Group Id\n0. Back");
        System.out.print("Choice [1/2/3/4/0] : ");
    }

    private void showGroupMenu() {
        System.out.println("====Menu Group====");
        System.out.println("1. Show Group\n2. Add New Group\n3. Delete Group\n0. Back");
        System.out.print("Choice [1/2/3/0] : ");
    }

    private void print(String str) {
        System.out.print(str);
    }

    private void println(String str) {
        System.out.println(str);
    }

    private void clearScreen() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {
            System.out.println("Failed to clear screen");
        }
    }

    public static void main(String[] args) {
        Main app = new Main();
        try {
            app.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
